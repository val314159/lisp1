(require 'asdf)
(push (directory-namestring *load-truename*)
      asdf:*central-registry*)
(require 'lisp1)

(defpackage :test-lisp1 (:use #:lisp1))
(in-package :test-lisp1)

(let+ hello ((x 10 (1+ x))
	     (y 20 (1- y))
	     (z 20))
      (format t "Z>>> ~A~%" (list x y z))
      (if (< x 20)
	  (hello)))

(print (macroexpand-1 '(let+ hello ((x 10 (1+ x))
				    (y 20 (1- y))
				    (z 20))
			(format t "Z>>> ~A~%" (list x y z))
			(if (< x 20)
			    (hello)))))

(if? 1 2 3)

(print #L:'(1))

(defvar we "ASDFDFG")

(print #L:we)
(print #L we)
(print #L:we)
