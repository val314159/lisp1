(defun qwert (qwert x)
  (if x
      (case (length x)
	(1 `(if ,@x nil))
	(2 `(if ,@x))
	(t `(if ,(car x) ,(cadr x)
		,(funcall qwert (cddr x)))))))

;;(defmacro cnd(&body b)
;;  (qwert #'qwert b))

;;(print (qwert '(1 2 3 4 5)))
;;(qw 1 2 3 4 5)
;;(print (me1 '(cnd 1 2 3 4 5)))

;;(print (cnd 1 2 3 4 5))

#|
(cl:defun doit (x)
    (cl:case #L:x
      (0 `nil)
      (1 `(cl:if ,@x nil))
      (2 `(cl:if ,@x))
      (t `(cl:if ,(cl:car x) ,(cl:cadr x) ,(doit (cl:cddr x))))
      ))

(defm (cnd &body b)
    (doit b))

(print (doit '(1 2 3 4 5)))
(print (cnd 1 2 3 4 5))

(defm (sf qaz)
    (declare (ignore qaz))
  (print "sdfsdfg")
  false)

(defn (test qaz)
    (declare (ignore qaz))
  ;;(print (uiop:getenv "PATH"))
  true)

(print true)

(cl:case 101
  (100)
  (tru (print "QWQWQ")))

(test 1)
|#
