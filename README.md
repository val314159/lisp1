# lisp1

lisp1

## What is it

## Quick install

## Quick Start

## Packages

### 

## future:

```lisp
(let(($ q w)
     ($ q w)	 
     ($ q w))
    (print 10)

let:: q := 1
      w := 2
      e := 3
    print 10

let*::q := 1
      w := 2
      e := 3
    print 10

let*:: x1 := 0 :> (+ x1 10)
       y2 :> (- 23 glob)
       z3 := 100
       w4 := 0 +...- 100
       v8 :=   -...+ 999
       df := 0 +...- 2000

let this-is-a-loop \
    :: x := 1
       z := 2
    print 100


1 ?> 2 !> 3 ?> 4 !> 0


```
---

### Example of use:

(require '#:lisp1)
(defpackage :example-lisp1 (:use #:lisp1))
(in-package :example-lisp1)

(print (macroexpand '(:3
		      (print "hello")
		      (print _1)
		      )))

(funcall (:3? (print "hello")
	      (print _1)))

```lisp
(load "lisp1")
(defpackage :xyz)
(in-package :xyz)
(import:all :cl)
```
