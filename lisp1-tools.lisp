(defpackage #:lisp1-tools (:use #:cl)
	    (:export #:defm #:call #:let+ #:if? #:if! #:??
		     #:defv #:print #:prinz #:prn #:pr #:fmt
		     #:defn #:nil #:t #:true #:false #:me1 #:cnd2
		     #:declare #:ignore #:ignorable #:iff #:cnd
		     &rest &body &optional)
	    (:shadow #:print #:if #:when #:unless #:cond #:funcall
		     #:lambda))
(in-package #:lisp1-tools)

(cl:set-macro-character #\λ (cl:lambda(s c) 'cl:lambda))
(cl:set-dispatch-macro-character
   #\# #\L (cl:lambda(s c d)
	     (cl:when (cl:eq (cl:peek-char nil s) #\:)
	       (cl:read-char s))
	     `(cl:length ,(cl:read s))))

(defun print (x) (cl:prin1 x) (cl:terpri)    x)
(defun prinz (x) (cl:princ x) (cl:terpri)    x)
(defun prn   (x) (cl:princ x) (cl:terpri)    x)
(defun pr    (x) (cl:princ x) (cl:princ " ") x)
(defun fmt (&rest r) (cl:apply #'cl:format t r))
(defun call(&rest r) (cl:apply #'cl:funcall r))

(defmacro ?if (  &rest b) `(cl:cond      ,@b))
(defmacro if? (a &rest b) `(cl:when   ,a ,@b))
(defmacro if! (a &rest b) `(cl:unless ,a ,@b))
(defmacro iff (a b c)     `(cl:if   ,a ,b ,c))

(defmacro me1 (%_) `(cl:macroexpand-1 ,%_))

(defmacro defm (%args &body %body)
  (cl:let*((%name (cl:pop %args)))
    `(progn
       (cl:defmacro ,%name ,%args ,@%body)
       (cl:defvar   ,%name (cl:macro-function ',%name))
       (cl:values  ',%name))))

(defmacro defn (%args &body %body)
  (cl:let*((%name (cl:pop %args)))
    `(progn
       (cl:defun    ,%name ,%args ,@%body)
       (cl:defvar   ,%name #',%name)
       (cl:values  ',%name))))

(defm (defv x y)
    `(cl:defvar ,x ,y))

(defv true  cl:t)
(defv false cl:nil)

(defmacro let+(name vlist &body body)
  (cl:let*((fn '#:fn)
	(get-last (cl:lambda (x) (cl:ecase (cl:length x)
				   (1 cl:nil)
				   (2 (cl:car x))
				   (3 (cl:caddr x)))))
	(mac `(cl:list* ',fn '#',fn ',(cl:mapcar get-last vlist))))
    `(cl:labels ((,fn ,(cl:list* fn (cl:mapcar #'cl:car vlist))
		   (cl:declare (cl:ignorable ,fn))
		   (cl:macrolet ((,name () ,mac)) ,@body)))
       (,fn #',fn ,@(cl:mapcar #'cl:cadr vlist)))))

(defun qwert (x)
  (case (length x)
    (0)
    (1 (list  (list (car x) nil)))
    (2 (list  (list (car x) (cadr x))))
    (t (list* (list (car x) (cadr x))
	      (qwert (cddr x))))))

(defun qwert2 (x)
  (case (length x)
    (0)
    (1 `((,(car x) nil)))
    (2 `((,(car x) ,(cadr x))))
    (t `((,(car x) ,(cadr x))
	       ,@(qwert2 (cddr x))))))

(defmacro cnd(&body b) (qwert b))
(defmacro cnd2(&body b) (qwert2 b))
