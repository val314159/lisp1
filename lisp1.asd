;;;; lisp1.asd
(defvar lisp1-files '("lisp1-import"
		      "lisp1-tools"
		      "lisp1-anons"
		      "lisp1-vfl"
		      "lisp1-all"))
(eval `(defsystem #:lisp1 :components
	 ,(mapcar (lambda (x)
		    (list :file x))
		  lisp1-files)))
